/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file request2.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#include "request2.h"
#include "htcpcp-tea.h"

namespace gtps {
std::ostream& operator<<(std::ostream& os, const RequestData& rd) {
		os << rd.method << " ";
		os << rd.request_target << " ";	
		os << rd.protocol;

		os << "\r\n";
		
		for(auto& [key,values] : rd.headers) {
			os << key << ": ";
			for(auto iter = values.begin(); iter != values.end(); ++iter) {
				os << *iter << ((iter+1)!=values.end()?",":"");
			}
			os << CONST::CRLN;
		}

		os << CONST::CRLN;

		os << rd.body;

		return os;
	}

		
	Request::Request(METHOD method, RequestTarget target, Headers headers, Body body, Protocol protocol) :
	requestData{method, target, protocol, CONST::protocol::HTCPCP_PROTOCOL_VERSION, headers, body}
	{

	}
	RequestData Request::request() const {
		return requestData;
	}

	void Request::setHeader(std::string key, std::vector<std::string> values) {
		requestData.headers.emplace(key, values);
	}

	void Request::addHeader(std::string key, std::string value) {
			if(requestData.headers.count(key)>0) {
				for(auto v : requestData.headers.at(key)) {
					if(v == value)
						return;
				}
				requestData.headers.at(key).push_back(value);
			} else {
				requestData.headers.emplace(key, std::vector<std::string> {value});
			}
		}

	void Request::addDefaultHaders() {
		  	addHeader("Date", currentTime());
			addHeader("Expires", currentTime());
            addHeader("Connection", "close");
			addHeader("Content-Length", std::to_string(requestData.body.length()));
	}
	void Request::contents(std::string content) {
		requestData.body = content;
	}



	GetRequest::GetRequest() : 
	Request{METHOD::GET, "/teapot/pot1", CONST::header::REQUEST_DEFAULT_HEADERS, "", Protocol::HTCPCP}
	{
			addDefaultHaders();
	}


	BrewRequest::BrewRequest() :
	Request{METHOD::BREW, "/teapot/pot1", CONST::header::REQUEST_DEFAULT_HEADERS, "START", Protocol::HTCPCP}
	{
		addDefaultHaders();
	}



	PropfindRequest::PropfindRequest() :
	Request{METHOD::PROPFIND, "/teapot/pot1", CONST::header::REQUEST_DEFAULT_HEADERS, "", Protocol::HTCPCP}
	{
		
		addDefaultHaders();
		
	}


	WhenRequest::WhenRequest() :
	Request{METHOD::WHEN, "/teapot/pot1", CONST::header::REQUEST_DEFAULT_HEADERS, "", Protocol::HTCPCP}
	{
		addDefaultHaders();
	}



	GetHttpRequest::GetHttpRequest() :
	Request{METHOD::GET, "/teapot/pot1", CONST::header::REQUEST_DEFAULT_HEADERS, "", Protocol::HTTP}
	{
		
		addDefaultHaders();
	}


}