/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file response.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

#include "response.h"
#include "htcpcp-tea.h"

namespace gtps {

    



	std::ostream& operator<<(std::ostream& os, const ResponseData& rd) {

		os << rd.protocol << " ";
		os << rd.code << CONST::CRLN;
		
		for(auto& [key,values] : rd.headers) {
			os << key << ": ";
			for(auto iter = values.begin(); iter != values.end(); ++iter) {
				os << *iter << ((iter+1)!=values.end()?",":"");
			}
			os << CONST::CRLN;
		}

		os << CONST::CRLN;

		os << rd.body;

		return os;
	}


		Response::Response(Status status, Headers headers, Body body, Protocol protocol) :
		responseData{status, headers, body, protocol} {
			
		}

		void Response::setHeader(std::string key, std::vector<std::string> values) {
			responseData.headers.emplace(key, values);
		}

		void Response::addHeader(std::string key, std::string value) {
			if(responseData.headers.count(key)>0) {
				for(auto v : responseData.headers.at(key)) {
					if(v == value)
						return;
				}
				responseData.headers.at(key).push_back(value);
			} else {
				responseData.headers.emplace(key, std::vector<std::string> {value});
			}
		}


		void Response::contents(std::string content) {
			responseData.body = content;
		}

        void Response::addDefaultHaders() {
            addHeader("Date", currentTime());
			addHeader("Expires", currentTime());
            addHeader("Connection", "close");
			addHeader("Age", std::to_string(0));
			addHeader("Content-Length", std::to_string(responseData.body.length()));
        }

		ResponseData Response::response() const {
			return responseData;
		}


		GetResponse::GetResponse() : 
			Response{CONST::status::OK}
        {
			

    
			std::stringstream ss;

			ss	<< "alfred-tea";

			contents(ss.str());

			addDefaultHaders();
			

		}


		BrewResponse::BrewResponse() :
		Response{Status{CONST::status::status_code::OK, CONST::status::reason_phrase::OK}}
        {
			contents("BREW STARTED.");
			addDefaultHaders();
		}



		PropfindResponse::PropfindResponse() :
		Response{Status{CONST::status::status_code::OK, CONST::status::reason_phrase::OK}}
        {
			
			addDefaultHaders();
			
			std::vector<std::string> milkType = {
				"cream",
				"half-and-half",
				"whole milk",
				"reduced fat milk",
				"low fat milk",
				"fat free milk",
				"lactose free whole milk",
				"lactose free reduced fat milk",
				"lactose free low fat milk",
				"lactose free fat free milk",
				"almond milk"
			};

			std::vector<std::string> sweetenerType = {
				"White Granulated Sugar",
				"Turbinado Unbleached Sugar",
				"Honey",
				"100% Mapple Syrup",
				"Agave Nectar",
				"Splenda (sucralose)",
				"Splenda (Stevia)",
				"Xylitol"
			};

			std::vector<std::string> teaType = {
				
			};


			setHeader("milk-type", milkType);
			setHeader("sweetener-type", sweetenerType);
			
		}


		WhenResponse::WhenResponse() :
		Response{Status{CONST::status::status_code::OK, CONST::status::reason_phrase::OK}}
        {
			contents("SOON (TM) or Soon-ish (TM) :)");
			addDefaultHaders();
		}

		
		ErrorResponse::ErrorResponse() :
        Response{Status{CONST::status::status_code::BAD_REQUEST, CONST::status::reason_phrase::BAD_REQUEST}}
		{
			contents("what do you think you're doing!");
			addDefaultHaders();
			
		}



		GetHttpResponse::GetHttpResponse() :
		Response{Status{CONST::status::status_code::IAM_A_TEAPOT, CONST::status::reason_phrase::IAM_A_TEAPOT}}
        {
			
			std::stringstream ss;
			responseData.protocol = Protocol::HTTP;
			ss	<< "<!DOCTYPE html>" << "\n"
				<< "<html>" << "\n"
				<< "	<head>" << "\n"
				<< "	<meta content=\"text/html;charset=utf-8\" http-equiv=\"Content-Type\">" << "\n"
				<< "	<meta content=\"utf-8\" http-equiv=\"encoding\">" << "\n"
				<< "	<title>HELLO FROM I AM A TEAPOT!</title>" << "\n"
				<< "</head>" << "\n"
				<< "	<body>" << "\n"
				<< "		<p>I AM A TEAPOT!</p>" << "\n"
				<< "		<p>\"I say let the world go to hell, but I should always have my tea\"<p>" << "\n"
				<< "		<p> ― Fyodor Dostoevsky</p>" << "\n"
				<< "	</body>" << "\n"
				<< "</html>" << "\n\n";

			contents(ss.str());
			addDefaultHaders();
		}



}
