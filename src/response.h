/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file response.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#pragma once


#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <sstream>
#include <ctime>
#include <chrono>
#include <iomanip>
#include <map>

#include "htcpcp-tea.h"


namespace gtps {

	struct ResponseData {
		Status code;
		Headers headers;
		Body body;
		Protocol protocol;
	};


	std::ostream& operator<<(std::ostream& os, const ResponseData& rd);


	class Response {
		public:

		
		Response(Status status, Headers headers = CONST::header::RESPONSE_DEFAULT_HEADERS, Body body = "", Protocol protocol = Protocol::HTCPCP);
		virtual ResponseData response() const;
		virtual ~Response() = default;

		public:

		virtual void setHeader(std::string key, std::vector<std::string> values);
		virtual void addHeader(std::string key, std::string value);
		virtual void addDefaultHaders();
		virtual void contents(std::string content);
        

		protected:
		
		ResponseData responseData;
		
	};


	class GetHttpResponse : public Response {
		public:

		GetHttpResponse();

	};

	class GetResponse : public Response {
		public:

		GetResponse();


	};


	class BrewResponse : public Response {
		public:

		BrewResponse();

	};


    class PropfindResponse : public Response {
		public:

		PropfindResponse();

	};


    class WhenResponse : public Response {
		public:

		WhenResponse();

	};

    class ErrorResponse : public Response {
		public:

		ErrorResponse();

	};
    

}