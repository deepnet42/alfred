/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file client.main.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#include <iostream>
#include <vector>

#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <algorithm>
#include <cstring>
#include <memory>

#include "htcpcp-tea.h"

#include "response.h"
#include "daemonize.h"
#include "request2.h"

#include "HTCPCP/requestParser.h"
#include "HTCPCP/requestLexer.h"

#include <antlr4-runtime/antlr4-runtime.h>


constexpr const int portno = 1313;





void my_handler(int s){
	std::cout << "[signal] caught signal " << s << "\n";
	exit(1); 
}

void header() {
	std::cout << "bruce-tea.client HTCPCP-TEA CLIENT VERSION 0\n";
}


int connect_retry(int sockid, const struct sockaddr* addr, socklen_t len) {
    while(true) {
        if(connect(sockid, addr, len)==0)
            return 0;
		std::cout << "retrying in 5s.\n";
        sleep(5);
    }
    return -1;
}
int main(int argc, char* argv[]) {

	header();


	if(argc < 2) {
		std::cerr << "error: missing IP address.\n";
		return EXIT_FAILURE;
	}

	std::string IPAddress = argv[1];
	
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = my_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);
	

	addrinfo hint = {};
	hint.ai_flags = AI_PASSIVE;
	hint.ai_family = AF_UNSPEC;
	hint.ai_socktype = SOCK_STREAM; 
	hint.ai_protocol = 0;
	hint.ai_addrlen = 0;
	hint.ai_canonname = NULL;
	hint.ai_addr = 0;
	hint.ai_next = NULL;


	struct addrinfo* ailist = {};
	if(int err = getaddrinfo(IPAddress.c_str(), std::to_string(portno).c_str(), &hint, &ailist); err!=0) {
		std::cerr << "error: getaddrinfo failed with " << gai_strerror(err) << std::endl;
		return EXIT_FAILURE;
	}

	struct sockaddr_in* serverAddress = NULL;
	const char* addr = NULL;
	int size = 0;
	char addressBuffer[INET_ADDRSTRLEN];

    struct addrinfo* aip = nullptr;
	for(addrinfo* iterator = ailist; iterator != NULL; iterator = iterator->ai_next) {
		if(iterator->ai_family == AF_INET and iterator->ai_socktype == SOCK_STREAM) {
			serverAddress = (struct sockaddr_in*)iterator->ai_addr;
			size = iterator->ai_addrlen;
			aip = iterator;
            addr = inet_ntop(AF_INET, &serverAddress->sin_addr, addressBuffer, INET_ADDRSTRLEN);
			std::cout << "address: " << (addr?addr:"unknown") << "\n";
			std::cout << "port   : " << (ntohs(serverAddress->sin_port)) << "\n";
			
		}
	}


	

   	while(true) {

		int socketid = socket(((struct sockaddr*)serverAddress)->sa_family, SOCK_STREAM, 0);
		if(socketid == -1) {
			std::cerr << "error: cannot open a socket." << std::endl;
			return EXIT_FAILURE;
		}



		std::cout << "[info] connecting   ip " << inet_ntoa(serverAddress->sin_addr) << "\n";
		std::cout << "[info] connecting port " << ntohs(serverAddress->sin_port) << "\n";
		


		int error;
		if(connect_retry(socketid, aip->ai_addr, aip->ai_addrlen) < 0) {
			error = errno;
			std::cerr << "[error] " << error;
			std::cerr << std::strerror(error);		
			shutdown(socketid, SHUT_RDWR);
			close(socketid);
			return 1;
		} else {
			// client work

			std::cout << "connected ... \n";
			
			std::vector<std::string> choices = 
			{
				"1. GET", "2. BREW START", "3. BREW STOP", "4. PROPFIND", "5. WHEN"
			};

			int choice = -1;

			while (choice == -1) {
				for(auto i : choices) {
					std::cout << i << "\n";
				}
				try {
					std::cout << "action: ";
					std::cin >> choice;
					
					if(std::cin.fail()) {
						std::cin.clear();
						std::cin.ignore();
						std::cerr << "invalid choice. \n";
						choice = -1;
					}

					if(choice <1 or choice >5) {
						choice = -1;
					}
				} catch(...) {
					std::cerr << "invalid choice. \n";
					choice = -1;
				}
			}
			

			std::unique_ptr<gtps::Request> req;
			switch (choice) {
				case 1:	req = std::make_unique<gtps::Request>(gtps::METHOD::GET, "/smartBrew/", gtps::CONST::header::REQUEST_DEFAULT_HEADERS, ""); break;
				case 2:	req = std::make_unique<gtps::Request>(gtps::METHOD::BREW, "/smartBrew/", gtps::CONST::header::REQUEST_DEFAULT_HEADERS, "START");break;
				case 3:	req = std::make_unique<gtps::Request>(gtps::METHOD::POST, "/smartBrew/", gtps::CONST::header::REQUEST_DEFAULT_HEADERS, "STOP");break;
				case 4:	req = std::make_unique<gtps::Request>(gtps::METHOD::PROPFIND, "/smartBrew/", gtps::CONST::header::REQUEST_DEFAULT_HEADERS, "");break;
				case 5:	req = std::make_unique<gtps::Request>(gtps::METHOD::WHEN, "/smartBrew/", gtps::CONST::header::REQUEST_DEFAULT_HEADERS, "");break;
			}
			std::stringstream ss;
			ss << req->request();
			std::string re = ss.str();
			std::cout << "sending " << re << "\n";
			send(socketid, re.c_str(), re.length(), 0);
			constexpr const int BUFFER_SIZE = 1024*8;
			char buffer[BUFFER_SIZE] = {};
			recv(socketid, buffer, sizeof(buffer)*sizeof(char), 0);
			std::cout << "recieved" << buffer << "\n";



			
			std::fill(buffer, buffer+BUFFER_SIZE, 0);

			shutdown(socketid, SHUT_RDWR);
    		close(socketid);
		}
	   }

	
    //shutdown(socketid, SHUT_RDWR);
    //close(socketid);
	freeaddrinfo(ailist);	
    return 0;
}


