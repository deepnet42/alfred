/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file htcpcp-tea.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
# include "htcpcp-tea.h"
# include <string>
# include <sstream>
# include <chrono>
# include <cstdlib>
# include <iomanip>
# include <utility>
# include <ctime>
# include <iostream>

namespace gtps {
	std::string currentTime() {
		auto now = std::chrono::system_clock::now();
		auto in_time_t = std::chrono::system_clock::to_time_t(now);

		std::stringstream ss;
		ss << std::put_time(std::gmtime(&in_time_t), "%a, %d %b %Y %H:%M:%S GMT");
		return ss.str();
	}


	std::ostream& operator<<(std::ostream& os, const METHOD& m) {
		switch(m) {
			case METHOD::GET:       os << CONST::method::GET; break;
			case METHOD::BREW:      os << CONST::method::BREW; break;
			case METHOD::POST:      os << CONST::method::POST; break;
			case METHOD::WHEN:      os << CONST::method::WHEN; break;
			case METHOD::PROPFIND:  os << CONST::method::PROPFIND; break;
			default:                os << "???"; break;
		}

		return os;
	}

	
	std::ostream& operator<<(std::ostream& os, const Protocol& p) {
		switch(p) {
			case Protocol::HTCPCP:  os << CONST::protocol::HTCPCP_PROTOCOL_NAME << "/" << CONST::protocol::HTCPCP_PROTOCOL_VERSION; break;
			case Protocol::HTTP:    os << CONST::protocol::HTTP_PROTOCOL_NAME << "/" << CONST::protocol::HTTP_PROTOCOL_VERSION; break;
			default:                os << "???"; break;
		}
		return os;
	}

	std::ostream& operator<<(std::ostream& os, const Status& s) {
		os << s.status_code << " " << s.reason_phrase;
		return os;
	}
}


