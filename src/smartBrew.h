/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file smartBrew.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

#pragma once

#include <string>
#include <optional>
#include <mutex>
#include <thread>
#include <iostream>

#include "htcpcp-tea.h"
#include "request2.h"
#include "response.h"


namespace gtps {

	
	enum class smartBrewState {
		STANDBY, BREWING, CANCEL
	};

	enum class smartBrewCommands {
		START, STOP, STATUS
	};



	class minismartBrew {
		public:

			minismartBrew();

			bool isReady();
			bool start();

			void stop();
			double brewEta() const;


			std::string brewEtaStr() const;

		private:

			std::chrono::time_point<std::chrono::system_clock> startBrewTime;
			std::chrono::time_point<std::chrono::system_clock> endBrewTime;

			smartBrewState state;
			std::mutex m;
			std::thread brew;
			std::thread abort;

	};


	class smartBrew {
		public:


		smartBrew();
		void loadConfig();

		std::optional<ResponseData> process(const RequestData& r);

		protected:


		std::optional<ResponseData> getHTTP(const RequestData& r);

		std::optional<ResponseData> get(const RequestData& r);
		std::optional<ResponseData> brew(const RequestData& r);
		std::optional<ResponseData> post(const RequestData& r);
		std::optional<ResponseData> propfind(const RequestData& r);
		std::optional<ResponseData> when(const RequestData& r);

		
		//std::optional<ResponseData> _200_OK(const RequestData& r);

		//std::optional<ResponseData> _400_BAD_REQUEST(const RequestData& r);
		std::optional<ResponseData> _404_NOT_FOUND(const RequestData& r);
		//std::optional<ResponseData> _406_NOT_ACCEPTABLE(const RequestData& r);
		//std::optional<ResponseData> _411_LENGTH_REQUIRED(const RequestData& r);
		//std::optional<ResponseData> _413_PAYLOAD_TOO_LARGE(const RequestData& r);
		//std::optional<ResponseData> _414_URI_TOO_LONG(const RequestData& r);
		//std::optional<ResponseData> _415_UNSUPORTED_MEDIA_TYPE(const RequestData& r);
		//std::optional<ResponseData> _418_IM_A_TEAPOT(const RequestData& r);
		//std::optional<ResponseData> _429_TOO_MANY_REQUESTS(const RequestData& r);


		//td::optional<ResponseData> _500_INTERNAL_SERVER_ERROR(const RequestData& r);
		std::optional<ResponseData> _501_NOT_IMPLEMENTED(const RequestData& r);
		//std::optional<ResponseData> _505_HTCPCP_VERSION_NOT_SUPPORTED(const RequestData& r);
		
		private:


		const std::string modelName       = {"alfred-tea"};
		const std::string modelVersion    = {"0"};
		

		std::string statusPage;
		std::string notfoundPage;
		std::vector<char> ico;
		
		minismartBrew brewer;
		

	};


}