/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file daemonize.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#include "daemonize.h"

#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <unistd.h>
#include <sys/stat.h>
#include <signal.h>

#include <iostream>
#include <cstring>

#include "common.h"

void daemonize() {

    int oldmask = umask(0);

    rlimit rl;
    if(getrlimit(RLIMIT_NOFILE, &rl) < 0) {
        gtps::log_error("getrlimit fail.");
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();

    if(pid < 0) {
        gtps::log_error("fork failed");
        exit(EXIT_FAILURE);
    } else if (pid != 0) {
        // parent
        gtps::log_info("parent goodbye. good luck my child");
        exit(EXIT_SUCCESS);
    } 
    
   
    // child
    /* handled elsewhere
    struct sigaction sa = {0};

    sa.sa_handler = SIG_IGN;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    
    int sigstatus = sigaction(SIGHUP, &sa, NULL);
    

    if(sigstatus < 0) {
        std::cerr << "[error] [child pid = " << pid << "] " << "sigaction failed to set SIGHUP.\n";
        exit(EXIT_FAILURE);  
    }
    */


   // lets do that again!
    pid = fork();

    if(pid < 0) {
        gtps::log_error("child pid = ", pid, "fork() failed");
        exit(EXIT_FAILURE);
    } else if (pid != 0) {
        // parent
        gtps::log_info("first child pid = ", pid, "goodbye. good luck my child!");
        exit(EXIT_SUCCESS);
    }


    int chdirstatus = chdir(gtps::SERVER_DIR);

    if(chdirstatus < 0) {
        gtps::log_error("child pid = ", pid, "chdir(gtps::SERVER_DIR) failed");
        gtps::log_error(std::strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(rl.rlim_max == RLIM_INFINITY) {
        rl.rlim_max = 1024;
    }

    
    for(int i = 0; i < rl.rlim_max; i++) {
        close(i);
    }


    int fd[3];
    fd[0] = open("/dev/null", O_RDWR);
    fd[1] = dup(0);
    fd[2] = dup(0);

    openlog("alfred-tea.server", LOG_CONS, LOG_DAEMON);

    for(int i = 0; i < 3; i++) {
        if (fd[i] != i) {
            syslog(LOG_ERR, "unexpceted file descriptor for fd[%d]=%d (should be fd[%d]=%d)", i, fd[i], i,i);
            exit(EXIT_FAILURE);
        }
    }

    syslog(LOG_INFO, "alfred-tea.server deamon has started...");
    
}