/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file common.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
# pragma once
# include <iostream>
# include <iomanip>

namespace gtps {



    template<typename... Args>
    void log_warning(Args... args) {

        std::clog << "[WARNING] " << " ";
        ((std::clog << " " << args), ...);
        std::cout << "\n";
    }

    template<typename... Args>
    void log_error(Args... args) {
        std::clog << "[ERROR  ] " << " ";
        ((std::cerr << " " << args), ...);
        std::cout << "\n";
    }

    template<typename... Args>
    void log_info(Args... args) {
        std::clog << "[INFO   ] " << " ";
        ((std::clog << " " << args), ...);
        std::cout << "\n";
    }

    template<typename... Args>
    void log_signal(Args... args) {
        std::clog << "[SIGNAL ] " << " ";
        ((std::clog << " " << args), ...);
        std::cout << "\n";
    }


    constexpr const char* SERVER_DIR = "htcpcp-tea";

   


};