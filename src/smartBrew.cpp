/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file smartBrew.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
# include "smartBrew.h"
# include "common.h"
# include "htcpcp-tea.h"
# include "request2.h"
# include "response.h"

# include <string>
# include <optional>
# include <fstream>
# include <iostream>
# include <cstring>
# include <cstdlib>
# include <string_view>
# include <thread>
# include <mutex>

# include <fstream>
# include <iterator>

namespace gtps {




	/**
	 * \brief   minismartBrew simulates a physical device that which will brew
	 * tea or coffee. It does not handle actual request processing. The device  
	 * only brews tea for five minutes. It can be extended for more advanced 
	 * functionality.
	 */


	/// MINI SMART BREW ///////////////////////////////////////////////////////
	/// MINI SMART BREW ///////////////////////////////////////////////////////
	/// MINI SMART BREW ///////////////////////////////////////////////////////

	minismartBrew::minismartBrew() : state{smartBrewState::STANDBY}, m{} {
		startBrewTime = {};
		endBrewTime = {};
	};

	bool minismartBrew::isReady() {
		bool ready = false;
		
		if(m.try_lock()) {
			{
				if(state == smartBrewState::STANDBY)
					ready = true;
			}
			m.unlock();
		}
		
		return ready;
	}

	bool minismartBrew::start() {
		if(m.try_lock()) {

			brew = std::thread(
				[&]() {
					if(state != smartBrewState::STANDBY)
						return;

					state 			= smartBrewState::BREWING;
					startBrewTime   = std::chrono::system_clock::now();
					endBrewTime     = startBrewTime + std::chrono::minutes(5);
					std::chrono::time_point timenow = startBrewTime;

					m.unlock();

					while(std::chrono::duration_cast<std::chrono::minutes>(endBrewTime - timenow).count()>=0) {
						// The purpose here is to simulate a real device taking the time to 
						// brew tea or coffee.
						timenow = std::chrono::system_clock::now();
						std::this_thread::sleep_for(std::chrono::seconds(1));
						if(state == smartBrewState::CANCEL) {
							m.lock();
								state = smartBrewState::STANDBY;
							m.unlock();
							return;
						}
					}

					m.lock();
						state = smartBrewState::STANDBY;
					m.unlock();
					
					
				}
			);

			brew.detach();
			return true;
		} 
		return false;
	}

	void minismartBrew::stop() {
		// brewing does not stop immediately; 
		// when the brew thread resumes from sleep and checks the flag it will 
		// self terminate
		this->abort = std::thread(
			[&]() {
				m.lock();
				{
					state = smartBrewState::CANCEL;
				}
				m.unlock();
			}	
		);

		this->abort.detach();
	}
			
	double minismartBrew::brewEta() const {
		// number of seconds to brewing completion
		std::chrono::time_point now = std::chrono::system_clock::now();
		return std::chrono::duration_cast<std::chrono::seconds>(endBrewTime - now).count();
	}


	std::string minismartBrew::brewEtaStr() const {
		// can be done better
		std::stringstream ss;
		double seconds = brewEta();

		int min = seconds / 60.0;
		int sec = seconds - (min * 60);


		ss  << ((min >= 0 and min <= 9)?"0":"") << min << ":" 
			<< ((sec >= 0 and sec <= 9)?"0":"") << sec;

		return ss.str();
	}



	/// MINI SMART BREW ///////////////////////////////////////////////////////
	/// MINI SMART BREW ///////////////////////////////////////////////////////
	/// MINI SMART BREW ///////////////////////////////////////////////////////



	/// SMART BREW ///////////////////////////////////////////////////////
	/// SMART BREW ///////////////////////////////////////////////////////
	/// SMART BREW ///////////////////////////////////////////////////////

		smartBrew::smartBrew() {
			loadConfig();
		}


		void smartBrew::loadConfig() {

			//////////////////// INIT STATUS PAGE ////////////////////
			// hard coded for now for proof of concept
			
			{
				const std::string statusFilename = "status.html";
				std::ifstream f(statusFilename, std::ios::in);

				if(!f or !f.is_open()) {
					log_error("404 NOT LOADED", "(", statusFilename, ")", "reason: ", std::strerror(errno));
					log_info("failover internal STATUS page.");

					
					statusPage="<!DOCTYPE html><html><head><title>418. I am a teapot</title></head><body><h1 style=\"color:red;margin-left: 64px;\">418. I am a teapot.</h1>{{{STATUS}}}</body></httml>";
				} else {
					log_info("STATUS PAGE LOADED", "(", statusFilename, ")");
					
					
					f.seekg(0, std::ios::end);
					statusPage.resize(f.tellg());
					f.seekg(0);
					f.read(statusPage.data(), statusPage.size());
					f.close();
				}
			}
			//////////////////// INIT STATUS PAGE ////////////////////



			//////////////////// INIT 404 PAGE ////////////////////
			{

				const std::string fourohfourFilename = "404.html";
				std::ifstream f(fourohfourFilename, std::ios::in);

				if(!f or !f.is_open()) {

					log_error("404 NOT LOADED", "(", fourohfourFilename, ")", "reason: ", std::strerror(errno));
					log_info("failover internal 404 page.");


					notfoundPage="<!DOCTYPE html><html><head><title>404. NOT FOUND</title></head><body><h1 style=\"color:red;margin-left: 64px;\">404. NOT FOUND.</h1></body></httml>";
				
				} else {
					
					log_info("404 PAGE LOADED", "(", fourohfourFilename, ")");
					
					f.seekg(0, std::ios::end);
					notfoundPage.resize(f.tellg());
					f.seekg(0);
					f.read(notfoundPage.data(), notfoundPage.size());
					f.close();
				}
			}
			//////////////////// INIT STATUS PAGE ////////////////////




			//////////////////// ICO ////////////////////
			{

				const std::string iconfile = "favicon.ico";
				std::ifstream f(iconfile, std::ios::in | std::ios::binary);

				if(!f or !f.is_open()) {

					log_error("ICON NOT LOADED", "(", iconfile, ")", "reason: ", std::strerror(errno));
					log_info("no failover");

				} else {
					
					log_info("ICON LOADED", "(", iconfile, ") (binary file)");
					
					f.unsetf(std::ios::skipws);
					std::streampos fileSize;

					f.seekg(0, std::ios::end);
					fileSize = f.tellg();
					f.seekg(0, std::ios::beg);

					ico.reserve(fileSize);

					ico.insert(ico.begin(), std::istream_iterator<char>(f), std::istream_iterator<char>());
					f.close();

				}
			}


			//////////////////// ICO ////////////////////


		}

		std::optional<ResponseData> smartBrew::process(const RequestData& r) {
			// http
			if( r.protocol  == Protocol::HTTP) {
				if(r.method==METHOD::GET)
					return getHTTP(r);
				auto res =  _501_NOT_IMPLEMENTED(r);
				return *res;
			}


			// htcpcp
			switch(r.method) {
				case METHOD::GET:       return get(r); break;
				case METHOD::BREW:      [[fallthrough]];
				case METHOD::POST:      return brew(r); break;
				case METHOD::PROPFIND:  return propfind(r); break;
				case METHOD::WHEN:      return when(r); break;
				default:                return _501_NOT_IMPLEMENTED(r); break;
			}

			return _501_NOT_IMPLEMENTED(r); // impossible to get here
		}



		std::optional<ResponseData> smartBrew::getHTTP(const RequestData& r) {
			
			// check for a valid http request
			if(r.request_target.empty() or r.request_target=="/") {


					std::string status_tmp = statusPage;
					std::string brewer_status = "Ready.";

					if(!brewer.isReady()) {
						brewer_status = "Please wait... Ready in ";
						brewer_status+= brewer.brewEtaStr();
					}


					std::string replacestr = "{{{STATUS}}}";

					if(status_tmp.find(replacestr)!=std::string::npos)
						status_tmp.replace(status_tmp.find(replacestr),replacestr.length(),brewer_status);

					auto res =  Response(CONST::status::IAM_A_TEAPOT,CONST::header::RESPONSE_DEFAULT_HEADERS,status_tmp,Protocol::HTTP);
					res.addDefaultHaders();
					return res.response();
			
			} else if(r.request_target == "/favicon.ico" and !ico.empty()) {

					std::string ico_;
					for(char c : ico) {
						ico_.push_back(c);
					}

					gtps::Headers headers = gtps::CONST::header::RESPONSE_DEFAULT_HEADERS;
					std::vector<std::string> content_values = {"image/x-icon"};
					
					headers.emplace(CONST::header::CONTENT_TYPE,  content_values);
					auto res = Response(CONST::status::OK,headers,ico_,Protocol::HTTP);
					res.addDefaultHaders();
					return res.response();
			}
			
			// 404 page
			auto res = Response(CONST::status::NOT_FOUND,CONST::header::RESPONSE_DEFAULT_HEADERS,notfoundPage,Protocol::HTTP);
			res.addDefaultHaders();
			return res.response();

		}

		std::optional<ResponseData> smartBrew::get(const RequestData& r) {
			if(r.request_target=="/smartBrew/" or r.request_target=="/smartBrew") {
				std::string message = "smartBrew is ";
				
				
				if(brewer.isReady()) {
					message += "Ready.";
				} else {
					message = "Please wait... ready in ";
					message += brewer.brewEtaStr();
				}
				
				return Response(CONST::status::OK,CONST::header::RESPONSE_DEFAULT_HEADERS,message,Protocol::HTCPCP).response();    
			}

			return _404_NOT_FOUND(r);
		}
		std::optional<ResponseData> smartBrew::brew(const RequestData& r) {
			if(r.request_target=="/smartBrew/" or r.request_target=="/smartBrew") {
				

				std::string message = "";
				Status status = CONST::status::OK;

				if(r.body == "START") {
					if(brewer.isReady()) {
						brewer.start();
						status = CONST::status::CREATED;
						message = "Brew started. Pick up in ";
						message += brewer.brewEtaStr();
					} else {
						status = CONST::status::CONFLICT;
						message = "Please wait ... Pick up in ";
						message += brewer.brewEtaStr();
						message += " or stop the request.";
					}
				} else if(r.body == "STOP") {
						brewer.stop();
						message = "Brewing will stop. Verify with a get request.";
				} else {
						message = "Missing command: Did you mean START, STOP";
						status = CONST::status::BAD_REQUEST;
				}

				return Response(status,CONST::header::RESPONSE_DEFAULT_HEADERS,message,Protocol::HTCPCP).response();    
			}

				
			

			return _404_NOT_FOUND(r);
		}
		std::optional<ResponseData> smartBrew::post(const RequestData& r) {
			return brew(r);
		}
		std::optional<ResponseData> smartBrew::propfind(const RequestData& r) {
			if(r.request_target=="/smartBrew/" or r.request_target=="/smartBrew") {
				
				auto propData = PropfindResponse().response();
				propData.body = "";

				return propData;
			}

			return _404_NOT_FOUND(r);
		}
		std::optional<ResponseData> smartBrew::when(const RequestData& r) {
			if(r.request_target=="/smartBrew/" or r.request_target=="/smartBrew") {
				std::string message;
				if(brewer.isReady()) {
					message = "Pick up tea or brew another one.";
				} else {
					message += "Tea is ready in ";
					message += brewer.brewEtaStr();
				}


				return Response(CONST::status::OK,CONST::header::RESPONSE_DEFAULT_HEADERS,message,Protocol::HTCPCP).response();    
			}
			return _404_NOT_FOUND(r);
		}


		std::optional<ResponseData> smartBrew::_404_NOT_FOUND(const RequestData& r) {
			return Response(CONST::status::NOT_FOUND,CONST::header::RESPONSE_DEFAULT_HEADERS,CONST::status::S_NOT_FOUND,Protocol::HTCPCP).response();
		}

		std::optional<ResponseData> smartBrew::_501_NOT_IMPLEMENTED(const RequestData& r) {
			return Response(CONST::status::NOT_IMPLEMENTED,CONST::header::RESPONSE_DEFAULT_HEADERS,CONST::status::S_NOT_IMPLEMENTED,Protocol::HTCPCP).response();
		}


		/// SMART BREW ///////////////////////////////////////////////////////
		/// SMART BREW ///////////////////////////////////////////////////////
		/// SMART BREW ///////////////////////////////////////////////////////
}
