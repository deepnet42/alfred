/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file request2.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#pragma once


#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <sstream>
#include <ctime>
#include <chrono>
#include <iomanip>
#include <map>

#include "htcpcp-tea.h"


namespace gtps {

	// yes there used to be request.h.
	// request2.h is correct

	using RequestTarget		= std::string;
	using ProtocolVersion	= std::string;

	struct RequestData {		
		METHOD method;
		RequestTarget request_target;
		Protocol protocol;
		ProtocolVersion version;
		Headers headers;
		Body body;
	};


	std::ostream& operator<<(std::ostream& os, const RequestData& rd);


	class Request {
		public:

		
		Request(METHOD method, RequestTarget target, Headers headers = CONST::header::REQUEST_DEFAULT_HEADERS, Body body = "", Protocol protocol = Protocol::HTCPCP);
		virtual RequestData request() const;
		virtual ~Request() = default;

		public:

		virtual void setHeader(std::string key, std::vector<std::string> values);
		virtual void addHeader(std::string key, std::string value);
		virtual void addDefaultHaders();
		virtual void contents(std::string content);
		

		protected:
		
		RequestData requestData;
		
	};

	class GetHttpRequest : public Request {
		public:

		GetHttpRequest();

	};

	class GetRequest : public Request {
		public:

		GetRequest();


	};


	class BrewRequest : public Request {
		public:

		BrewRequest();

	};


	class PropfindRequest : public Request {
		public:

		PropfindRequest();

	};


	class WhenRequest : public Request {
		public:

		WhenRequest();

	};

	class ErrorRequest : public Request {
		public:

		ErrorRequest();

	};
	

}