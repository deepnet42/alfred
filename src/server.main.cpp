/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file server.main.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <algorithm>
#include <optional>
#include <cstdlib>
#include <cstring>


#include "htcpcp-tea.h"
#include "response.h"
#include "daemonize.h"

#include "HTCPCP/requestParser.h"
#include "HTCPCP/requestLexer.h"
#include <antlr4-runtime/antlr4-runtime.h>
#include "smartBrew.h"
#include "common.h"



constexpr const char* 	ip				= "127.0.0.1";
constexpr const int 	portno 			= 1313;
constexpr const int		maxRequestSize	= 1460; // bytes


//#define DAEMONIZE

// global so can be accessed by SIGNAL handlers
gtps::smartBrew* smartBrewController = nullptr;


void header();

// SIGNAL HANDLERS
void sigint_handler(int s);
void sigpipe_handler(int s);
void sighub_handler(int s);
void sigterm_handler(int s);
// SIGNAL HANDLERS

// disptach request (threaded)
void handleRequest(int socketid, gtps::smartBrew& smartBrewSystem);

int main(int argc, char* argv[]) {


	header();

	std::string ipaddress = ip;
	if(argc > 1) {
		ipaddress = argv[1];
	}

	#ifdef DAEMONIZE
		daemonize();	// Experimental
	#else
		if(chdir(gtps::SERVER_DIR) < 0) {
			gtps::log_error("can't find config directory.\n");
			return EXIT_FAILURE;
		}
	#endif
		

	// signal handler stuff
	struct sigaction sigIntHandler;
	struct sigaction sigPipeHandler;
	struct sigaction sigTermHandler;
	struct sigaction sigHupHandler;

	sigIntHandler.sa_handler = sigint_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigPipeHandler.sa_handler = sigpipe_handler;
	sigemptyset(&sigPipeHandler.sa_mask);
	sigPipeHandler.sa_flags = 0;


	sigTermHandler.sa_handler = sigterm_handler;
	sigemptyset(&sigTermHandler.sa_mask);
	sigTermHandler.sa_flags = 0;


	sigHupHandler.sa_handler = sighub_handler;
	sigemptyset(&sigHupHandler.sa_mask);
	sigHupHandler.sa_flags = SA_RESTART;

	sigaction(SIGINT,	&sigIntHandler, NULL);
	sigaction(SIGPIPE,	&sigPipeHandler, NULL);
	sigaction(SIGTERM,	&sigTermHandler, NULL);
	sigaction(SIGHUP,	&sigHupHandler, NULL);
	// signal handler stuff

	

	gtps::smartBrew smartBrewSystem;
	smartBrewController = &smartBrewSystem;


	// socket stuff
	addrinfo hint 		= {};
	hint.ai_flags 		= AI_PASSIVE;
	hint.ai_family 		= AF_UNSPEC;
	hint.ai_socktype 	= SOCK_STREAM; 
	hint.ai_protocol 	= 0;
	hint.ai_addrlen 	= 0;
	hint.ai_canonname 	= NULL;
	hint.ai_addr 		= 0;
	hint.ai_next 		= NULL;
	// socket stuff


	struct addrinfo* ailist = {};
	

	if(int err = getaddrinfo(ipaddress.c_str(), std::to_string(portno).c_str(), &hint, &ailist); err!=0) {
		gtps::log_error("getaddrinfo failed with ", err, " more info below:");
		gtps::log_error(gai_strerror(err));
		return EXIT_FAILURE;
	}

	struct sockaddr_in* serverAddress	= NULL;
	const char* addr					= NULL;
	int size 							= 0;

	char addressBuffer[INET_ADDRSTRLEN];

	
	for(addrinfo* iterator = ailist; iterator != NULL; iterator = iterator->ai_next) {
		if(iterator->ai_family == AF_INET and iterator->ai_socktype == SOCK_STREAM) {
			serverAddress = (struct sockaddr_in*)iterator->ai_addr;
			size = iterator->ai_addrlen;
			addr = inet_ntop(AF_INET, &serverAddress->sin_addr, addressBuffer, INET_ADDRSTRLEN);
			
			gtps::log_info("address", (addr?addr:"unknown"));
			gtps::log_info("   port", (ntohs(serverAddress->sin_port)));

			
		}
	}


	int socketid = socket(((struct sockaddr*)serverAddress)->sa_family, SOCK_STREAM, 0);
	if(socketid == -1) {

		gtps::log_error("cannot open a socket.");
		gtps::log_error(std::strerror(errno));
		return EXIT_FAILURE;
	
	}


	sockaddr_in clientAddress = {};
	
	int bindStatus = bind(socketid, (struct sockaddr*) serverAddress, size);
	if(bindStatus == -1) {


		gtps::log_error("cannot bind a socket.");
		gtps::log_error(std::strerror(errno));
		
		shutdown(socketid, SHUT_RDWR);
		close(socketid);

		return EXIT_FAILURE;
	}


	if(listen(socketid, 1024) < 0) {

		gtps::log_error("cannot listen on the socket");
		gtps::log_error(std::strerror(errno));
		
		shutdown(socketid, SHUT_RDWR);
		close(socketid);

		return EXIT_FAILURE;
		
	}


	


	gtps::log_info("listening on address ", inet_ntoa(serverAddress->sin_addr));
	gtps::log_info("listening on    port", ntohs(serverAddress->sin_port));

	socklen_t clientLen = sizeof(clientAddress);
	freeaddrinfo(ailist);

	while(true) {
			
			gtps::log_info("waiting for client connection...");

			int acceptSocketid = accept(socketid, (sockaddr*) &clientAddress, &clientLen);

			if(acceptSocketid == -1) {

				gtps::log_error("accept failed");
				gtps::log_error(std::strerror(errno));
				
				shutdown(socketid, SHUT_RDWR);
				close(socketid);
				return EXIT_FAILURE;

			}


			gtps::log_info(	"incoming connection from ", 
							inet_ntoa(clientAddress.sin_addr), ":", 
							ntohs(clientAddress.sin_port));

			std::thread handleRequestThread(handleRequest, acceptSocketid,
											std::ref(smartBrewSystem));
			
			handleRequestThread.detach();


		}

		shutdown(socketid, SHUT_RDWR);
		close(socketid);

	return EXIT_SUCCESS;
}




void handleRequest(int socketid, gtps::smartBrew& smartBrewSystem) {
		
		constexpr int hugebuffersize = maxRequestSize;
		char hugebuffer[hugebuffersize] = {};
		std::string hb;

		/*while(true)*/ // can implement this in the future. usually all requests
						// should fit into the buffer size;
		{
			int size = recv(socketid, hugebuffer, hugebuffersize, 0);

			if(size == -1) {

				gtps::log_error("cannot read from client");
				gtps::log_error(std::strerror(errno));
				
			} else if (size == 0) {
				gtps::log_info("client sent zero data");
			} else {

				gtps::log_info(size, " bytes read) data = \n", hugebuffer, "\n\n\n"); 
				
				hb = hugebuffer;
			}
			
			std::fill(hugebuffer, hugebuffer+hugebuffersize, 0);
		}


		if(hb.empty()) {
			std::stringstream ss; ss << gtps::ErrorResponse().response();
			gtps::log_error("bad request.");
			send(socketid, ss.str().c_str(), ss.str().length(), 0);
		} else {
			try {
				antlr4::ANTLRInputStream input(hb);
				requestLexer rLexer(&input);
				antlr4::CommonTokenStream tokens(&rLexer);
				tokens.fill();
				requestParser rParser(&tokens);
				
				/*antlr4::ParserRuleContext* ri = */rParser.request();
				gtps::RequestData rd = rParser.rd;

				auto resdata = smartBrewSystem.process(rd);
				std::stringstream ss;
				ss << *resdata;

				send(socketid, ss.str().c_str(), ss.str().length(), 0);


			} catch(...) {
				
				std::stringstream ss;
				ss << gtps::ErrorResponse().response();
				std::cout << gtps::ErrorResponse().response();
				
				gtps::log_error("parse failed");
				send(socketid, ss.str().c_str(), ss.str().length(), 0);
				
			}
			
		}
		
		
		shutdown(socketid, SHUT_RDWR);
		close(socketid);
	}




void header() {
	std::cout << "GTPS HTCPCP-TEA SERVER VERSION 0\n";
	std::cout << "Config:\n";
	std::cout << "max buffer size: " << maxRequestSize << "\n";
	std::cout << "::WARNNING:: ::WARNNING:: ::WARNNING:: " << "\n";
	std::cout << "::WARNNING:: ::WARNNING:: ::WARNNING:: " << "\n";
	std::cout << "::WARNNING:: ::WARNNING:: ::WARNNING:: " << "\n";

	std::cout << "USE ONLY ON A PRIVATE LOCAL NETWORK. THIS IS A DEMO.\n";
	std::cout << "FOR TESTING ONLY ON A PRIVATE LOCAL NETWORK\n";
	std::cout << "NOT HARDENED AGAINST MILICIOUS CLIENTS\n" << "\n";

	std::cout << "::WARNNING:: ::WARNNING:: ::WARNNING::\n" << "\n";
	std::cout << "::WARNNING:: ::WARNNING:: ::WARNNING:: " << "\n";
	std::cout << "::WARNNING:: ::WARNNING:: ::WARNNING:: " << "\n";
}

void sigint_handler(int s){
	gtps::log_signal("SIGINT (exit(1))");
	exit(EXIT_SUCCESS); 
}


void sigpipe_handler(int s) {
	gtps::log_signal("SIGPIPE (ignore)");
}

void sighub_handler(int s) {
	gtps::log_signal("SIGHUB (reloading config)");
	if(smartBrewController) {
		gtps::log_signal("SIGHUB (reloaded)");
		smartBrewController->loadConfig();
	}
}

void sigterm_handler(int s) {
	gtps::log_signal("SIGTERM (exit(1))");
	exit(1);
}
