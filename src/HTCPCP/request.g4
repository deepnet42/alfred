grammar request;


@postinclude {
#include "../request2.h"
}

@members
{
gtps::RequestData rd;
}


request	: method WS target WS protocol SLASH version CRLN (headers CRLN)* CRLN commands?;


method:		get 		
			| brew		
			| post		
			| propfind
			| when;	
//text: ANYTHING;

//fragment
//ANYTHING: (.)* ;

headers: field_name ':' (WS)? field_value
{
std::vector<std::string> s;
s.push_back($field_value.text);
rd.headers.emplace($field_name.text, s);

}
;


field_name: ((ALPHA | DIGIT)+ ('-')?)+;
field_value: (get | brew | post | propfind | when | http | htcpcp | ALPHA | DIGIT | SLASH | '*' | ',' | ';' | '=' | '.' | '(' | ')' | '+' | '-' | '_' | ':' | WS)+;


get:		'GET'
{rd.method = gtps::METHOD::GET;}
;

brew:		'BREW'
{rd.method = gtps::METHOD::BREW;}
;

post:		'POST'
{rd.method = gtps::METHOD::POST;}
;

propfind: 	'PROPFIND'
{rd.method = gtps::METHOD::PROPFIND;}
;

when: 		'WHEN'
{rd.method = gtps::METHOD::WHEN;}
;

target:	path
{rd.request_target = $path.text;}
;

path: (SLASH segment)+ SLASH?;
segment: (ALPHA | DIGIT | DASH | DOT | '~' | '$' | '&' | '\'' | '"' | '(' | ')' | '*'| ',' | ';' | '=' | '!' | ':' | '@' | '?' | '_' | HEX)*; 

version: pversion
{rd.version = $pversion.text;}
;

pversion: DIGIT+ (DOT DIGIT+)?;


protocol: http | htcpcp;

http: 'HTTP'
{
	rd.protocol = gtps::Protocol::HTTP;
}
;

htcpcp: 'HTCPCP'
{
	rd.protocol = gtps::Protocol::HTCPCP;
}
;

commands: (start | stop | status)?;


start: 'START'
{rd.body = "START";}
;

stop: 'STOP'
{rd.body = "STOP";}
;

status: 'STATUS'
{rd.body = "STATUS";}
;


WS: ' ';
SLASH: '/';
CRLN: (CR)* NL;
CR: '\r';
NL: '\n';
ALPHA: [A-Za-z];
DIGIT: [0-9]; 
DASH: '-';
DOT: '.';
HEX: '%'([0-9]|[A-F])+;