/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file htcpcp-tea.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#pragma once


#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <sstream>
#include <ctime>
#include <chrono>
#include <iomanip>
#include <map>





namespace gtps {

	std::string currentTime();


	using Headers 	= 	std::map<std::string, std::vector<std::string> >;

	enum class METHOD {
		GET, BREW, POST, PROPFIND, WHEN
	};


	using Body 	=	std::string;
	
	enum class Protocol {
		HTTP, HTCPCP
	};


	struct Status {
		int status_code;
		std::string reason_phrase;
	};


	std::ostream& operator<<(std::ostream& os, const METHOD& m);
	std::ostream& operator<<(std::ostream& os, const Protocol& p);
	std::ostream& operator<<(std::ostream& os, const Status& s);

	namespace CONST {


		namespace protocol {
			constexpr const char* HTCPCP_PROTOCOL_NAME		=   "HTCPCP";
			constexpr const char* HTCPCP_PROTOCOL_VERSION	=   "1.0";
	 
	 		constexpr const char* HTPCPCP_PROTOCOL_1_0		=	"HTCPCP/1.0";

			constexpr const char* HTTP_PROTOCOL_NAME		=   "HTTP";
			constexpr const char* HTTP_PROTOCOL_VERSION		=   "1.1";

			constexpr const char* HTTP_PROTOCOL_1_1		=	"HTCPCP/1.1";
		}

		namespace method {
			const std::string BREW  	=	"BREW";
			const std::string POST		=	"POST";
			const std::string GET		=	"GET";
			const std::string PROPFIND	=	"PROPFIND";
			const std::string WHEN		=	"WHEN";
		}

		namespace header {
			const std::string SAFE  			=	"Safe";
			const std::string ACCEPT_ADDITIONS	=	"Accept-Additions";
			const std::string ADDITIONS_TYPE	=	"addition-type";
			const std::string MILK_TYPE			=	"milk-type";
			const std::string SYRUP_TYPE		=	"syrup-type";
			const std::string ALCOHOL_TYPE		=	"alcohol-type";
			const std::string CONTENT_TYPE		=	"content-type";



			const Headers RESPONSE_DEFAULT_HEADERS = {
				{"Server", {"gtps"}},
				{"Content-Type", {"text/html; charset=utf-8"}},
				{"Cache-Control", {"no-cache"}},
				{"Accept-Language", {"*"}},
				{"Access-Control-Allow-Methods", {"GET", "BREW", "POST", "PROPFIND", "WHEN"}}
			};


			const Headers REQUEST_DEFAULT_HEADERS = {
				{"User-Agent", {"TEACUP/1.0 (HTCPCP/1.0)"}},
				{"Accept", {"text/html; charset=utf-8"}},
				{"Accept-Language", {"en-US,en"}},
				{"Cache-Control", {"max-age=0"}}
			};


		}

		namespace status {
			namespace status_code {
				
				const constexpr int OK						= 200;
				const constexpr int CREATED					= 201;

				const constexpr int BAD_REQUEST				= 400;
				const constexpr int NOT_FOUND				= 404;
				const constexpr int METHOD_NOT_ALLOWED		= 405;
				const constexpr int NOT_ACCETABLE			= 406;
				const constexpr int CONFLICT				= 409;
				const constexpr int IAM_A_TEAPOT			= 418;


				const constexpr int INTERNAL_SERVER_ERROR	= 500;
				const constexpr int NOT_IMPLEMENTED			= 501;
				const constexpr int HTCPCP_VERSION_NOT_SUPPORTED	= 505;
			}

			namespace reason_phrase {
				const std::string OK  					= "OK";
				const std::string CREATED				= "Created";
				
				const std::string BAD_REQUEST			= "Bad Request";
				const std::string NOT_FOUND				= "Not Found";
				const std::string METHOD_NOT_ALLOWED	= "Method Not Allowed";
				const std::string NOT_ACCETABLE			= "Not Acceptable";
				const std::string CONFLICT				= "Conflict";
				const std::string IAM_A_TEAPOT			= "I\'m a teapot";

				const std::string INTERNAL_SERVER_ERROR	= "Internal Server Error";
				const std::string NOT_IMPLEMENTED		= "Not Implemented";
				const std::string HTCPCP_VERSION_NOT_SUPPORTED	= "HTCPCP Version Not Supported";
				
				
			}

			const std::string S_OK 							= "200 OK";
			const std::string S_CREATED 					= "201 Created";
			const std::string S_BAD_REQUEST 				= "400 Bad Request";
			const std::string S_NOT_FOUND		 			= "404 Not Found";
			const std::string S_METHOD_NOT_ALLOWED 			= "405 Method Not Allowed";
			const std::string S_NOT_ACCETABLE 				= "406 Not Acceptable";
			const std::string S_CONFLICT					= "409 Conflict";
			const std::string S_IAM_A_TEAPOT 				= "418 I'm a teapot";
			const std::string S_INTERNAL_SERVER_ERROR 		= "500 Internal Server Error";
			const std::string S_NOT_IMPLEMENTED	 			= "501 Not Implemented";
			const std::string S_HTCPCP_VERSION_NOT_SUPPORTED= "405 HTCPCP Version Not Supported";

			
			const Status OK 					= {status_code::OK, reason_phrase::OK};
			const Status CREATED 				= {status_code::CREATED, reason_phrase::CREATED};
			const Status BAD_REQUEST 			= {status_code::BAD_REQUEST, reason_phrase::BAD_REQUEST};
			const Status NOT_FOUND				= {status::status_code::NOT_FOUND, status::reason_phrase::NOT_FOUND};
			const Status METHOD_NOT_ALLOWED 	= {status_code::METHOD_NOT_ALLOWED, reason_phrase::METHOD_NOT_ALLOWED};
			const Status NOT_ACCETABLE	 		= {status_code::NOT_ACCETABLE, reason_phrase::NOT_ACCETABLE};
			const Status CONFLICT 				= {status_code::CONFLICT, reason_phrase::CONFLICT};
			const Status IAM_A_TEAPOT 			= {status_code::IAM_A_TEAPOT, reason_phrase::IAM_A_TEAPOT};
			const Status INTERNAL_SERVER_ERROR 	= {status_code::INTERNAL_SERVER_ERROR, reason_phrase::INTERNAL_SERVER_ERROR};
			const Status NOT_IMPLEMENTED 		= {status_code::NOT_IMPLEMENTED, reason_phrase::NOT_IMPLEMENTED};
			const Status HTCPCP_VERSION_NOT_SUPPORTED 	= {status_code::HTCPCP_VERSION_NOT_SUPPORTED, reason_phrase::HTCPCP_VERSION_NOT_SUPPORTED};
		}

		namespace message {
			const std::string COFFEE_POT  			=	"coffeepot";
			const std::string TEA_POT  				=	"teapot";
		}

		namespace comands {
			const std::string START  				=	"START";
			const std::string STOP  				=	"STOP";
			const std::string STATUS  				=	"STATUS";
		}

		
		constexpr const char* CRLN = "\r\n";
		

	}

};