# Introduction

Alfred is a *proof of concept appliance* for brewing tea over the Hyper Text Coffee Pot Control Protocol (HTCPCP) and HTCPCP-TEA. See [RFC2324](https://datatracker.ietf.org/doc/html/rfc2324) and [RFC7168](https://www.rfc-editor.org/rfc/rfc7168).


Is it a suitable protocol for such applications? Well, that remains to be seen, but if hindsight 20/20 has something to say, HTCPCP has predicted such devices: The Internet of Insecure Things.


While humorous, it is a great way to learn about the nature of all things related to internet communication protocols and seeing the under the hood low level technical details of such technology in action.

Projects like this one are small enough to be implemented by a single individual in a short span of time, yet these types of projects still represent the individual contribution to a much larger work, such as the HTTP protocol, in a team.

Fun, creative, and borderline ridiculous ideas such as **HTCPCP**, **IP over Avian Carriers**, and countless of others are at the heart of imagining the future and testing ideas to their limit. This kind of playful and creative atmosphere allows for innovation and creates a cultivating culture, which is vital for the future generation of young individuals going into STEM.

To learn more please read the Wikipedia entry: 
https://en.wikipedia.org/wiki/Hyper_Text_Coffee_Pot_Control_Protocol


# Build 

Builds on any Linux distribution but has dependencies on the following: 

* ANTLR v4
* CMake
* GCC

See [BUILD-INSTRUCTIONS.md](BUILD-INSTRUCTIONS.md)

# Examples

Client request
```
$./bruce-tea.client [redacted]
bruce-tea.client HTCPCP-TEA CLIENT VERSION 0
address: [redacted]
port   : 1313
[info] connecting   ip [redacted]
[info] connecting port 1313
connected ... 
1. GET
2. BREW START
3. BREW STOP
4. PROPFIND
5. WHEN
action: 2
sending BREW /smartBrew/ HTCPCP/1.0
Accept: text/html; charset=utf-8
Accept-Language: en-US,en
Cache-Control: max-age=0
User-Agent: TEACUP/1.0 (HTCPCP/1.0)

START
recievedHTCPCP/1.0 201 Created
Accept-Language: *
Access-Control-Allow-Methods: GET,BREW,POST,PROPFIND,WHEN
Cache-Control: no-cache
Content-Type: text/html; charset=utf-8
Server: gtps

Brew started. Pick up in 04:59
```

Server response
```
$./alfred-tea.server [redacted]
GTPS HTCPCP-TEA SERVER VERSION 0
Config:
max buffer size: 1460
::WARNNING:: ::WARNNING:: ::WARNNING:: 
::WARNNING:: ::WARNNING:: ::WARNNING:: 
::WARNNING:: ::WARNNING:: ::WARNNING:: 
USE ONLY ON A PRIVATE LOCAL NETWORK. THIS IS A DEMO.
FOR TESTING ONLY ON A PRIVATE LOCAL NETWORK
NOT HARDENED AGAINST MILICIOUS CLIENTS

::WARNNING:: ::WARNNING:: ::WARNNING::

::WARNNING:: ::WARNNING:: ::WARNNING:: 
::WARNNING:: ::WARNNING:: ::WARNNING:: 
[INFO   ]   STATUS PAGE LOADED ( status.html )
[INFO   ]   404 PAGE LOADED ( 404.html )
[INFO   ]   ICON LOADED ( favicon.ico ) (binary file)
[INFO   ]   address [redacted]
[INFO   ]      port 1313
[INFO   ]   listening on address  [redacted]
[INFO   ]   listening on    port 1313
[INFO   ]   waiting for client connection...
[INFO   ]   incoming connection from  [redacted] : 60184
[INFO   ]   waiting for client connection...
[INFO   ]   160  bytes read) data = 
 BREW /smartBrew/ HTCPCP/1.0
Accept: text/html; charset=utf-8
Accept-Language: en-US,en
Cache-Control: max-age=0
User-Agent: TEACUP/1.0 (HTCPCP/1.0)

START 
```


# Notes 
Alfred is written in C++ and uses ANTLR v4 (ANother Tool for Language Recognition) parser generator to parse a subset of HTTP requests. The latter can be done by hand using a recursive descent parser. Alfred uses network sockets. Alfred runs on any Linux distribution, but can be modified to run on Windows (with some modifications as Alfred uses system calls specific to *nix based systems)

# License

MPL 2.0



(c) 2022 DeepNet42. All Rights Reserved.

Updated Version.


https://www.deepnet42.com
https://www.deepnet42.com/projects/#alfred-htcpcp-htcpcp-tea

```
______  _______ _______  _____  __   _ _______ _______     
|     \ |______ |______ |_____] | \  | |______    |        
|_____/ |______ |______ |       |  \_| |______    |        
                                                           
Deepnet42
```
